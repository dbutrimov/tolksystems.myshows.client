﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TolkSystems.MyShows.Client;
using System.Text;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Diagnostics;
using TolkSystems.MyShows;
using TolkSystems.MyShows.Model;
using System.Threading;

namespace TestClient
{
    [TestClass]
    public class TestClient
    {
        public IAuthorization GetAuthorization()
        {
            return new NativeAuthorization("demo", CalcMD5Hash("demo"));
        }

        [TestMethod]
        public async Task TestAuthorization()
        {
            var client = new MyShowsClient(GetAuthorization());
            await client.AuthorizeAsync();
        }

        [TestMethod]
        public async Task TestReAuthorization()
        {
            var client = new MyShowsClient(GetAuthorization());
            await client.GetProfileAsync();
        }

        [TestMethod]
        public async Task TestMultiRequests()
        {
            var client = new MyShowsClient(GetAuthorization());

            var tasks = new Task[] 
            {
                client.GetProfileAsync(),
                client.GetShowsAsync(),
                MyShowsClient.GetTopShowsAsync(TopSerialGender.All),
                MyShowsClient.GetShowAsync(1),
                client.GetWatchedEpisodesAsync(1)
            };

            await Task.WhenAll(tasks);
        }



        public static string CalcMD5Hash(string input)
        {
            return CalcMD5Hash(input, Encoding.Default);
        }

        public static string CalcMD5Hash(string input, Encoding encoding)
        {
            using (var md5 = MD5.Create())
            {
                var inputBytes = encoding.GetBytes(input);
                var hash = md5.ComputeHash(inputBytes);

                var sb = new StringBuilder();
                foreach (var b in hash)
                    sb.Append(b.ToString("x2"));

                return sb.ToString();
            }
        }
    }
}
