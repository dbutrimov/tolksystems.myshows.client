﻿using System;
namespace TolkSystems.MyShows.Model
{
    public interface IProfile : IProfileBase
    {
        ProfileBase[] Followers { get; }
        ProfileBase[] Friends { get; }
        ProfileStats Stats { get; }
    }
}
