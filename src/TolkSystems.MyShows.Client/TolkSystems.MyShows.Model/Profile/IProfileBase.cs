﻿using System;
namespace TolkSystems.MyShows.Model
{
    public interface IProfileBase
    {
        string Avatar { get; }
        Gender Gender { get; }
        string Login { get; }
        int WastedTime { get; }
    }
}
