﻿using Newtonsoft.Json;
using System;

namespace TolkSystems.MyShows.Model
{
	public class Profile : ProfileBase, IProfile, IProfileBase
	{
        [JsonProperty("friends")]
        public ProfileBase[] Friends { get; set; }

        [JsonProperty("followers")]
        public ProfileBase[] Followers { get; set; }

        [JsonProperty("stats")]
        public ProfileStats Stats { get; set; }
	}
}
