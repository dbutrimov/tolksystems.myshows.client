﻿using Newtonsoft.Json;
using System;
using TolkSystems.MyShows.Model.Converters;

namespace TolkSystems.MyShows.Model
{
	public class ProfileBase : IProfileBase
    {
        #region Properties
        [JsonProperty("login")]
        public string Login { get; set; }

        [JsonProperty("avatar")]
        public string Avatar { get; set; }

        [JsonProperty("wastedTime")]
        public int WastedTime { get; set; }

        [JsonProperty("gender")]
        [JsonConverter(typeof(GenderConverter))]
        public Gender Gender { get; set; }
        #endregion
	}
}
