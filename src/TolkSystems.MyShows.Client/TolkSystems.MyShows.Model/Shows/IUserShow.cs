﻿using System;
namespace TolkSystems.MyShows.Model
{
    public interface IUserShow : IShowBase
    {
        double Rating { get; }
        int? Runtime { get; }
        int TotalEpisodes { get; }
        int WatchedEpisodes { get; }
        WatchStatus WatchStatus { get; }
    }
}
