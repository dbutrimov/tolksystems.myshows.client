﻿using Newtonsoft.Json;
using System;
using TolkSystems.MyShows.Model.Converters;

namespace TolkSystems.MyShows.Model
{
    public class UserShow : ShowBase, IUserShow, IShowBase
	{
		#region Properties

        [JsonProperty("runtime")]
        public int? Runtime { get; set; }

        [JsonProperty("watchStatus")]
        [JsonConverter(typeof(SerialWatchStatusConverter))]
        public WatchStatus WatchStatus { get; set; }

        [JsonProperty("watchedEpisodes")]
        public int WatchedEpisodes { get; set; }

        [JsonProperty("totalEpisodes")]
        public int TotalEpisodes { get; set; }

        [JsonProperty("rating")]
        public double Rating { get; set; }

		#endregion
	}
}
