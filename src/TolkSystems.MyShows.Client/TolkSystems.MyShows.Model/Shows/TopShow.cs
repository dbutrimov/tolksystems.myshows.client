﻿using Newtonsoft.Json;
using System;
using TolkSystems.MyShows.Model.Converters;

namespace TolkSystems.MyShows.Model
{
    public class TopShow : ShowBase, ITopShow, IShowBase
	{
		[JsonProperty("id")]
		public new int ShowID
		{
			get { return base.ShowID; }
			set { base.ShowID = value; }
		}

		[JsonProperty("status")]
		[JsonConverter(typeof(SerialShowStatusConverter))]
		public new ShowStatus ShowStatus
		{
			get { return base.ShowStatus; }
			set { base.ShowStatus = value; }
		}

        [JsonProperty("rating")]
        public double Rating { get; set; }

        [JsonProperty("year")]
        public int? Year { get; set; }

        [JsonProperty("voted")]
        public int? Voted { get; set; }

        [JsonProperty("watching")]
        public int? Watching { get; set; }

        [JsonProperty("place")]
        public int Place { get; set; }
	}
}
