﻿using System;
namespace TolkSystems.MyShows.Model
{
    public interface ITopShow : IShowBase
    {
        int Place { get; }
        double Rating { get; }
        //int ShowID { get; }
        //ShowStatus ShowStatus { get; }
        int? Voted { get; }
        int? Watching { get; }
        int? Year { get; }
    }
}
