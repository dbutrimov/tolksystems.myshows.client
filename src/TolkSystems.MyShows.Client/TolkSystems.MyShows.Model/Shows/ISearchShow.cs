﻿using System;
namespace TolkSystems.MyShows.Model
{
    public interface ISearchShow : IShowBase
    {
        string Country { get; }
        DateTime? Ended { get; }
        int[] Genres { get; }
        int? IMDBID { get; }
        int? KinopoiskID { get; }
        double Rating { get; }
        int? Runtime { get; }
        //int ShowID { get; }
        //ShowStatus ShowStatus { get; }
        DateTime? Started { get; }
        int? TVRageID { get; }
        int? Voted { get; }
        int? Watching { get; }
        int? Year { get; }
    }
}
