﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using TolkSystems.MyShows.Model.Converters;

namespace TolkSystems.MyShows.Model
{
    public class Show : SearchShow, IShow, IShowBase //SerialBase
    {
        [JsonProperty("id")]
        public new int ShowID
        {
            get { return base.ShowID; }
            set { base.ShowID = value; }
        }

        [JsonProperty("status")]
        [JsonConverter(typeof(SerialShowStatusConverter))]
        public new ShowStatus ShowStatus
        {
            get { return base.ShowStatus; }
            set { base.ShowStatus = value; }
        }

		/*private double _rating = 0;
        [JsonProperty("rating")]
		public double Rating
		{
			get { return _rating; }
			set
			{
				if (value != _rating)
				{
					var old = _rating;
					_rating = value;
					OnPropertyChanged(new PropertyChangedEventArgs2("Rating", old, _rating));
				}
			}
		}

		private string _country = null;
        [JsonProperty("country")]
		public string Country
		{
			get { return _country; }
			set
			{
				if (value != _country)
				{
					var old = _country;
					_country = value;
					OnPropertyChanged(new PropertyChangedEventArgs2("Country", old, _country));
				}
			}
		}

		private DateTime? _started = null;
        [JsonProperty("started")]
		[JsonConverter(typeof(DateTimeConverter))]
		public DateTime? Started // Nov/16/2004"
		{
			get { return _started; }
			set
			{
				if (value != _started)
				{
					var old = _started;
					_started = value;
					OnPropertyChanged(new PropertyChangedEventArgs2("Started", old, _started));
				}
			}
		}

		private DateTime? _ended = null;
        [JsonProperty("ended")]
		[JsonConverter(typeof(DateTimeConverter))]
		public DateTime? Ended // May/21/2012
		{
			get { return _ended; }
			set
			{
				if (value != _ended)
				{
					var old = _ended;
					_ended = value;
					OnPropertyChanged(new PropertyChangedEventArgs2("Ended", old, _ended));
				}
			}
		}

		private int? _year = 0;
        [JsonProperty("year")]
		public int? Year
		{
			get { return _year; }
			set
			{
				if (value != _year)
				{
					var old = _year;
					_year = value;
					OnPropertyChanged(new PropertyChangedEventArgs2("Year", old, _year));
				}
			}
		}

		private int? _kinopoiskID = 0;
        [JsonProperty("kinopoiskId")]
		public int? KinopoiskID
		{
			get { return _kinopoiskID; }
			set
			{
				if (value != _kinopoiskID)
				{
					var old = _kinopoiskID;
					_kinopoiskID = value;
					OnPropertyChanged(new PropertyChangedEventArgs2("KinopoiskID", old, _kinopoiskID));
				}
			}
		}

		private int? _tvRageID = 0;
        [JsonProperty("tvrageId")]
		public int? TVRageID
		{
			get { return _tvRageID; }
			set
			{
				if (value != _tvRageID)
				{
					var old = _tvRageID;
					_tvRageID = value;
					OnPropertyChanged(new PropertyChangedEventArgs2("TVRageID", old, _tvRageID));
				}
			}
		}

		private int? _imdbID = 0;
        [JsonProperty("imdbId")]
		public int? IMDBID
		{
			get { return _imdbID; }
			set
			{
				if (value != _imdbID)
				{
					var old = _imdbID;
					_imdbID = value;
					OnPropertyChanged(new PropertyChangedEventArgs2("IMDBID", old, _imdbID));
				}
			}
		}

		private int? _watching = null;
		[JsonProperty("watching")]
		public int? Watching
		{
			get { return _watching; }
			set
			{
				if (value != _watching)
				{
					var old = _watching;
					_watching = value;
					OnPropertyChanged(new PropertyChangedEventArgs2("Watching", old, _watching));
				}
			}
		}

		private int? _voted = null;
        [JsonProperty("voted")]
		public int? Voted
		{
			get { return _voted; }
			set
			{
				if (value != _voted)
				{
					var old = _voted;
					_voted = value;
					OnPropertyChanged(new PropertyChangedEventArgs2("Voted", old, _voted));
				}
			}
		}

		private int? _runtime = null;
        [JsonProperty("runtime")]
		public int? Runtime
		{
			get { return _runtime; }
			set
			{
				if (value != _runtime)
				{
					var old = _runtime;
					_runtime = value;
					OnPropertyChanged(new PropertyChangedEventArgs2("Runtime", old, _runtime));
				}
			}
		}*/

        [JsonProperty("episodes")]
        public Dictionary<int, ShowEpisode> Episodes { get; set; }

		/*private int[] _genres = null;
        [JsonProperty("genres")]
		public int[] Genres
		{
			get { return _genres; }
			set
			{
				if (value != _genres)
				{
					var old = _genres;
					_genres = value;
					OnPropertyChanged(new PropertyChangedEventArgs2("Genres", old, _genres));
				}
			}
		}*/
    }
}
