﻿using System;

namespace TolkSystems.MyShows.Model
{
	public enum WatchStatus
	{
		Unknown = 0, // нет данных / не смотрю
		Watching = 1, // watching
		Later = 2, // later
		Cancelled = 4, // cancelled
		Finished = 8  // finished
	}
}
