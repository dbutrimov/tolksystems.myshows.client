﻿using Newtonsoft.Json;
using System;
using TolkSystems.MyShows.Model.Converters;

namespace TolkSystems.MyShows.Model
{
    public class ShowBase : IShowBase
	{
		#region Properties
        [JsonProperty("showId")]
        public int ShowID { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("ruTitle")]
        public string RuTitle { get; set; }

        [JsonProperty("showStatus")]
        [JsonConverter(typeof(SerialShowStatusConverter))]
        public ShowStatus ShowStatus { get; set; }

        [JsonProperty("image")]
        public Uri Image { get; set; }
		#endregion

		#region Comparison
		public override bool Equals(object obj)
		{
			if (obj == null || obj.GetType() != GetType())
				return false;

			return ((ShowBase)obj).ShowID == ShowID;
		}

		public static bool operator ==(ShowBase sserial1, ShowBase serial2)
		{
			if (object.ReferenceEquals(sserial1, serial2))
				return true;

			if ((object)sserial1 == null || (object)serial2 == null)
				return false;

			return sserial1.Equals(serial2);
		}

		public static bool operator !=(ShowBase serial1, ShowBase serial2)
		{
			return !(serial1 == serial2);
		}

		public override int GetHashCode()
		{
			return base.GetHashCode();
		}
		#endregion
	}
}
