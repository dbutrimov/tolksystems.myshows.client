﻿using Newtonsoft.Json;
using System;
using TolkSystems.MyShows.Model.Converters;

namespace TolkSystems.MyShows.Model
{
    public class SearchShow : ShowBase, ISearchShow, IShowBase
	{
		[JsonProperty("id")]
		public new int ShowID
		{
			get { return base.ShowID; }
			set { base.ShowID = value; }
		}

		[JsonProperty("status")]
		[JsonConverter(typeof(SerialShowStatusConverter))]
		public new ShowStatus ShowStatus
		{
			get { return base.ShowStatus; }
			set { base.ShowStatus = value; }
		}

        [JsonProperty("rating")]
        public double Rating { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }

        [JsonProperty("started")]
        [JsonConverter(typeof(DateTimeConverter))]
        public DateTime? Started { get; set; }

        [JsonProperty("ended")]
        [JsonConverter(typeof(DateTimeConverter))]
        public DateTime? Ended { get; set; }

        [JsonProperty("year")]
        public int? Year { get; set; }

        [JsonProperty("kinopoiskId")]
        public int? KinopoiskID { get; set; }

        [JsonProperty("tvrageId")]
        public int? TVRageID { get; set; }

        [JsonProperty("imdbId")]
        public int? IMDBID { get; set; }

        [JsonProperty("watching")]
        public int? Watching { get; set; }

        [JsonProperty("voted")]
        public int? Voted { get; set; }

        [JsonProperty("runtime")]
        public int? Runtime { get; set; }

        [JsonProperty("genres")]
        public int[] Genres { get; set; }
	}
}
