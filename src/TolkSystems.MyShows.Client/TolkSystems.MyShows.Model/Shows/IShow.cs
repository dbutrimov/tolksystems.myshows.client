﻿using System;
using System.Collections.Generic;

namespace TolkSystems.MyShows.Model
{
    public interface IShow : IShowBase
    {
        Dictionary<int, ShowEpisode> Episodes { get; }
    }
}
