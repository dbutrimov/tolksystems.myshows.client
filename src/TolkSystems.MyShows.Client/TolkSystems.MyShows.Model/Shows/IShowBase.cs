﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TolkSystems.MyShows.Model
{
    public interface IShowBase
    {
        int ShowID { get; }
        string Title { get; }
        string RuTitle { get; }
        ShowStatus ShowStatus { get; }
        Uri Image { get; }
    }
}
