﻿using System;

namespace TolkSystems.MyShows.Model
{
	public enum ShowStatus
	{
        None,                   // нет данных
        CanceledOrEnded,        // "Canceled/Ended" - завершен
        FinalSeason,            // "Final Season" - последний сезон
        NeverAired,             // "Never Aired" - не был в эфире
        NewSeries,              // "New Series" - новые серии
        OnHiatus,               // "On Hiatus" - приостановлен
        ReturningSeries,        // "Returning Series" - снимается
        TBDOrOnTheBubble        // "TBD/On The Bubble" - не объявлено
	}
}
