﻿using System;
namespace TolkSystems.MyShows.Model
{
    public interface IGenre
    {
        int ID { get; }
        string RuTitle { get; }
        string Title { get; }
    }
}
