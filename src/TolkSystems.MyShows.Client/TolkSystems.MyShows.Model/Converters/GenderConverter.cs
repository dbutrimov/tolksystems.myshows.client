﻿using Newtonsoft.Json;
using System;
using TolkSystems.MyShows.Model;

namespace TolkSystems.MyShows.Model.Converters
{
	public class GenderConverter : JsonConverter
	{
		public static Gender ParseGender(string str)
		{
			Gender result = Gender.Unknown;

			if (!string.IsNullOrEmpty(str))
			{
				if (string.Compare(str, "m", StringComparison.OrdinalIgnoreCase) == 0)
					result = Gender.Male;
				else if (string.Compare(str, "f", StringComparison.OrdinalIgnoreCase) == 0)
					result = Gender.Female;
			}

			return result;
		}


		public override bool CanConvert(Type objectType)
		{
			return (objectType == typeof(Gender));
		}

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			if (reader.Value == null)
				return Gender.Unknown;

			string str = reader.Value.ToString();
			return ParseGender(str);
		}

		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			string name = Enum.GetName(typeof(Gender), value);
			writer.WriteValue(Char.ToLower(name[0]));
		}
	}
}
