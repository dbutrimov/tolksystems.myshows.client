﻿using Newtonsoft.Json;
using System;
using TolkSystems.MyShows.Model;

namespace TolkSystems.MyShows.Model.Converters
{
	public class NewsActionConverter : JsonConverter
	{
		public static NewsAction ParseAction(string str)
		{
			if (string.Compare(str, "watch", StringComparison.OrdinalIgnoreCase) == 0)
				return NewsAction.Watch;

			throw new Exception(string.Format("Invalid action: {0}", str));
		}


		public override bool CanConvert(Type objectType)
		{
			return (objectType == typeof(NewsAction));
		}

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			if (reader.Value == null)
				return NewsAction.Unknown;

			string str = reader.Value.ToString();
			return ParseAction(str);
		}

		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			NewsAction status = (NewsAction)value;
			writer.WriteValue(status.ToString().ToLower());
		}
	}
}
