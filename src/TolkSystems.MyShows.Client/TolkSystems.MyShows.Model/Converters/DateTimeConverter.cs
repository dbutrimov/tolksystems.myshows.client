﻿using Newtonsoft.Json;
using System;
using System.Globalization;

namespace TolkSystems.MyShows.Model.Converters
{
	public class DateTimeConverter : JsonConverter
	{
		public override bool CanConvert(Type objectType)
		{
			return (objectType == typeof(DateTime?));
		}

		private static CultureInfo _culture = new CultureInfo("ru-RU");

        public static DateTime Parse(string str)
        {
            return DateTime.Parse(str, _culture, DateTimeStyles.None);
        }

        public static bool TryParse(string str, out DateTime result)
        {
            result = DateTime.MinValue;
            return DateTime.TryParse(str, _culture, DateTimeStyles.None, out result);
        }

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			if (reader.Value == null)
				return null;

			string str = reader.Value.ToString();
			DateTime date = DateTime.MinValue;
			if (TryParse(str, out date))
				return date;

			return null;
		}

		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			DateTime? date = (DateTime?)value;
			if (date == null)
				writer.WriteNull();
			else
				writer.WriteValue(date.Value.ToString(_culture));
		}
	}
}
