﻿using Newtonsoft.Json;
using System;
using System.Linq;
using System.Collections.Generic;
using TolkSystems.MyShows.Model;

namespace TolkSystems.MyShows.Model.Converters
{
	public class SerialShowStatusConverter : JsonConverter
	{
		#region ShowStatus dictionary
		private readonly static Dictionary<string, ShowStatus> _showStatuses = new Dictionary<string, ShowStatus>()
        {
            { "Canceled/Ended", ShowStatus.CanceledOrEnded },
            { "Final Season", ShowStatus.FinalSeason },
            { "Never Aired", ShowStatus.NeverAired },
            { "New Series", ShowStatus.NewSeries },
            { "On Hiatus", ShowStatus.OnHiatus },
            { "Returning Series", ShowStatus.ReturningSeries },
            { "TBD/On The Bubble", ShowStatus.TBDOrOnTheBubble }
        };

		public static ShowStatus ParseShowStatus(string str)
		{
			ShowStatus result = ShowStatus.None;
			_showStatuses.TryGetValue(str, out result);
			return result;
		}

		public static string ShowStatusToString(ShowStatus status)
		{
			return _showStatuses.FirstOrDefault(x => x.Value == status).Key;
		}
		#endregion


		public override bool CanConvert(Type objectType)
		{
			return (objectType == typeof(ShowStatus));
		}

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			if (reader.Value == null)
				return ShowStatus.None;

			string str = reader.Value.ToString();
			return ParseShowStatus(str);
		}

		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			ShowStatus status = (ShowStatus)value;
			writer.WriteValue(ShowStatusToString(status));
		}
	}
}
