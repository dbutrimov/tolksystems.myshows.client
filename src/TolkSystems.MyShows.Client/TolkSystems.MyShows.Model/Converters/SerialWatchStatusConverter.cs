﻿using Newtonsoft.Json;
using System;
using System.Linq;
using System.Collections.Generic;
using TolkSystems.MyShows.Model;

namespace TolkSystems.MyShows.Model.Converters
{
	public class SerialWatchStatusConverter : JsonConverter
	{
		#region WatchState dictionary
		private readonly static Dictionary<string, WatchStatus> _watchStatuses = new Dictionary<string, WatchStatus>()
        {
            { "watching", WatchStatus.Watching },
            { "later", WatchStatus.Later },
            { "cancelled", WatchStatus.Cancelled },
            { "finished", WatchStatus.Finished }
        };

		public static WatchStatus ParseWatchStatus(string str)
		{
			WatchStatus result = WatchStatus.Unknown;
			if (_watchStatuses.ContainsKey(str))
				result = _watchStatuses[str];

			return result;
		}

		public static string WatchStatusToString(WatchStatus status)
		{
			return _watchStatuses.FirstOrDefault(x => x.Value == status).Key;
		}
		#endregion


		public override bool CanConvert(Type objectType)
		{
			return (objectType == typeof(WatchStatus));
		}

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			if (reader.Value == null)
				return WatchStatus.Unknown;

			string str = reader.Value.ToString();
			return ParseWatchStatus(str);
		}

		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			WatchStatus status = (WatchStatus)value;
			writer.WriteValue(WatchStatusToString(status));
		}
	}
}
