﻿using Newtonsoft.Json;
using System;
using TolkSystems.MyShows.Model.Converters;

namespace TolkSystems.MyShows.Model
{
	public class NewsItem : INewsItem
	{
		#region Properties
		[JsonProperty("episodeId")]
		public int? EpisodeID { get; set; }

		[JsonProperty("showId")]
		public int? ShowID { get; set; }

		[JsonProperty("show")]
		public string Show { get; set; } // Serial title

		[JsonProperty("title")]
		public string Title { get; set; } // Episode

		[JsonProperty("login")]
		public string Login { get; set; }

		[JsonProperty("gender")]
		[JsonConverter(typeof(GenderConverter))]
		public Gender Gender { get; set; }

		[JsonProperty("episodes")]
		public int? Episodes { get; set; }

		[JsonProperty("episode")]
		public string Episode { get; set; } //s02e02

		[JsonProperty("action")]
		[JsonConverter(typeof(NewsActionConverter))]
		public NewsAction Action { get; set; } //watch

		//[JsonProperty("date")]
		//public DateTime Date { get; set; }
		#endregion
	}
}
