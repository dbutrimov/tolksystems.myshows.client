﻿using System;
namespace TolkSystems.MyShows.Model
{
    public interface INewsItem
    {
        NewsAction Action { get; }
        string Episode { get; }
        int? EpisodeID { get; }
        int? Episodes { get; }
        Gender Gender { get; }
        string Login { get; }
        string Show { get; }
        int? ShowID { get; }
        string Title { get; }
    }
}
