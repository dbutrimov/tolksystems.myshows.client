﻿using Newtonsoft.Json;
using System;

namespace TolkSystems.MyShows.Model
{
	public class Genre : IGenre 
	{
        [JsonProperty("id")]
        public int ID { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("ruTitle")]
        public string RuTitle { get; set; }
	}
}
