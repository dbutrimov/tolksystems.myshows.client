﻿using System;

namespace TolkSystems.MyShows.Model
{
    public interface IShowEpisode
    {
        DateTime? AirDate { get; }
        int EpisodeID { get; }
        int EpisodeNumber { get; }
        Uri Image { get; }
        string ProductionNumber { get; }
        int SeasonNumber { get; }
        int SequenceNumber { get; }
        string ShortName { get; }
        string Title { get; }
        string TVRageLink { get; }
    }
}
