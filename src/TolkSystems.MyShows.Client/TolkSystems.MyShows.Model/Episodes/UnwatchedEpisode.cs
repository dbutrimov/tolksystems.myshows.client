﻿using System;

namespace TolkSystems.MyShows.Model
{
    public class UnwatchedEpisode : Episode, IUnwatchedEpisode, IEpisodeBase
    {
    }
}
