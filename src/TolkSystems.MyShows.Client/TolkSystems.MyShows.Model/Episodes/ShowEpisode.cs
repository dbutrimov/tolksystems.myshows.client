﻿using Newtonsoft.Json;
using System;
using TolkSystems.MyShows.Model.Converters;

namespace TolkSystems.MyShows.Model
{
    public class ShowEpisode : EpisodeBase, IShowEpisode, IEpisodeBase
	{
		[JsonProperty("id")]
        public new int EpisodeID
        {
            get { return base.EpisodeID; }
            set { base.EpisodeID = value; }
        }
		
		//[JsonProperty("showId")]
		//public int ShowID { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }
		
        [JsonProperty("airDate")]
        [JsonConverter(typeof(DateTimeConverter))]
        public DateTime? AirDate { get; set; }

        [JsonProperty("shortName")]
        public string ShortName { get; set; }

        [JsonProperty("tvrageLink")]
        public string TVRageLink { get; set; }

        [JsonProperty("image")]
        public Uri Image { get; set; }

        [JsonProperty("seasonNumber")]
        public int SeasonNumber { get; set; }

        [JsonProperty("episodeNumber")]
        public int EpisodeNumber { get; set; }

        [JsonProperty("productionNumber")]
        public string ProductionNumber { get; set; }

        [JsonProperty("sequenceNumber")]
        public int SequenceNumber { get; set; }
	}
}
