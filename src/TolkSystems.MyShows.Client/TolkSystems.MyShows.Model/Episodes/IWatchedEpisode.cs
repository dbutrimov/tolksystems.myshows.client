﻿using System;
namespace TolkSystems.MyShows.Model
{
    public interface IWatchedEpisode : IEpisodeBase
    {
        //int EpisodeID { get; }
        double? Rating { get; }
        DateTime WatchDate { get; }
    }
}
