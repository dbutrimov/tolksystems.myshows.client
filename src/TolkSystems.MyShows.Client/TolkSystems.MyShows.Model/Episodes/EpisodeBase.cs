﻿using Newtonsoft.Json;
using System;

namespace TolkSystems.MyShows.Model
{
	public class EpisodeBase : IEpisodeBase
	{
        [JsonProperty("episodeId")]
        public int EpisodeID { get; set; }

		#region Comparison
		public override bool Equals(object obj)
		{
			if (obj == null || obj.GetType() != GetType())
				return false;

            return ((EpisodeBase)obj).EpisodeID == EpisodeID;
		}

		public static bool operator ==(EpisodeBase episode1, EpisodeBase episode2)
		{
			if (object.ReferenceEquals(episode1, episode2))
				return true;

			if (((object)episode1 == null) || ((object)episode2 == null))
				return false;

			return episode1.Equals(episode2);
		}

		public static bool operator !=(EpisodeBase episode1, EpisodeBase episode2)
		{
			return !(episode1 == episode2);
		}

		public override int GetHashCode()
		{
			return base.GetHashCode();
		}
		#endregion
	}
}
