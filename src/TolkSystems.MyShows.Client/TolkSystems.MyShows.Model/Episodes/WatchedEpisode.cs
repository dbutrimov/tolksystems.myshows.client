﻿using Newtonsoft.Json;
using System;
using TolkSystems.MyShows.Model.Converters;

namespace TolkSystems.MyShows.Model
{
	public class WatchedEpisode : EpisodeBase, IWatchedEpisode, IEpisodeBase
	{
		[JsonProperty("id")]
		public new int EpisodeID 
		{
			get { return base.EpisodeID; }
			set { base.EpisodeID = value; }
		}

        [JsonProperty("watchDate")]
        [JsonConverter(typeof(DateTimeConverter))]
        public DateTime WatchDate { get; set; }

        [JsonProperty("rating")]
        public double? Rating { get; set; }
	}
}
