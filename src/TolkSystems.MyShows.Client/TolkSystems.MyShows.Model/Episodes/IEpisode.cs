﻿using System;
namespace TolkSystems.MyShows.Model
{
    public interface IEpisode : IEpisodeBase
    {
        DateTime? AirDate { get; }
        int EpisodeNumber { get; }
        int SeasonNumber { get; }
        int ShowID { get; }
        string Title { get; }
    }
}
