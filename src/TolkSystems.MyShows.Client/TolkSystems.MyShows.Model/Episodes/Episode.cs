﻿using Newtonsoft.Json;
using System;
using TolkSystems.MyShows.Model.Converters;

namespace TolkSystems.MyShows.Model
{
    public class Episode : EpisodeBase, IEpisode, IEpisodeBase
	{
        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("showId")]
        public int ShowID { get; set; }

        [JsonProperty("seasonNumber")]
        public int SeasonNumber { get; set; }

        [JsonProperty("episodeNumber")]
        public int EpisodeNumber { get; set; }

        [JsonProperty("airDate")]
        [JsonConverter(typeof(DateTimeConverter))]
        public DateTime? AirDate { get; set; }
	}
}
