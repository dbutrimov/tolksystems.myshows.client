﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TolkSystems.Core;
using TolkSystems.MyShows.Model;
using TolkSystems.MyShows.Model.Converters;

namespace TolkSystems.MyShows.Client
{
    public class MyShowsClient : DisposableBase
    {
        public const string JsonMimeType = "application/json";
        public static readonly Uri BaseAddress = new Uri("http://api.myshows.me");


        private IAuthorization _authorization;
        private IAuthorizator _authorizator;

        private HttpClient _innerClient;
        private HttpClientHandler _innerClientHandler;

        public MyShowsClient(IAuthorization authorization)
        {
            authorization.ThrowIfArgumentNull("authorization");

            _authorization = authorization;
           
            _innerClientHandler = new HttpClientHandler() 
            { 
                AllowAutoRedirect = true, 
                UseCookies = true,
                CookieContainer = new CookieContainer() 
            };

            _innerClient = new HttpClient(_innerClientHandler);
            _innerClient.BaseAddress = BaseAddress;
            _innerClient.DefaultRequestHeaders.Accept.Clear();
            _innerClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(JsonMimeType));
            _innerClient.DefaultRequestHeaders.IfModifiedSince = DateTime.UtcNow;

            _authorizator = new MyShowsAuthorizator(_authorization, _innerClient, _innerClientHandler);
        }

        static MyShowsClient()
        {
            var clientHandler = new HttpClientHandler()
            {
                AllowAutoRedirect = true,
                UseCookies = true,
                CookieContainer = new CookieContainer()
            };

            _sharedClient = new HttpClient(clientHandler);
            _sharedClient.BaseAddress = BaseAddress;
            _sharedClient.DefaultRequestHeaders.Accept.Clear();
            _sharedClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(JsonMimeType));
            _sharedClient.DefaultRequestHeaders.IfModifiedSince = DateTime.UtcNow;
        }

        #region Authorization
        public Task AuthorizeAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            return _authorization.AuthorizeAsync(_innerClient, cancellationToken);
        }
        #endregion

        #region Authorized requests
        public async Task<Profile> GetProfileAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            var result = await _innerClient.GetObjectAsync<Profile>(
                new Uri("/profile/", UriKind.Relative), cancellationToken, _authorizator);
            return result;
        }

        public async Task<Dictionary<int, UserShow>> GetShowsAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            var result = await _innerClient.GetObjectAsync<Dictionary<int, UserShow>>(
                new Uri("/profile/shows/", UriKind.Relative), cancellationToken, _authorizator);
            return result;  
        }

        public async Task<Dictionary<int, WatchedEpisode>> GetWatchedEpisodesAsync(int showID, CancellationToken cancellationToken = default(CancellationToken))
        {
            var result = await _innerClient.GetObjectAsync<Dictionary<int, WatchedEpisode>>(
                UriBuilder.Build(UriKind.Relative, "/profile/shows/{0}/", showID), cancellationToken, _authorizator);
            return result;
        }

        public async Task<Dictionary<int, UnwatchedEpisode>> GetUnwatchedEpisodesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            var result = await _innerClient.GetObjectAsync<Dictionary<int, UnwatchedEpisode>>(
                new Uri("/profile/episodes/unwatched/", UriKind.Relative), cancellationToken, _authorizator);
            return result;
        }

        public async Task<Dictionary<int, NextEpisode>> GetNextEpisodesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            var result = await _innerClient.GetObjectAsync<Dictionary<int, NextEpisode>>(
                new Uri("/profile/episodes/next/", UriKind.Relative), cancellationToken, _authorizator);
            return result;
        }

        public async Task CheckEpisodeAsync(int episodeID, CancellationToken cancellationToken = default(CancellationToken), int rating = 0)
        {
            Uri uri = rating > 0 
                ? UriBuilder.Build(UriKind.Relative, "/profile/episodes/check/{0}?rating={1}", episodeID, rating)
                : UriBuilder.Build(UriKind.Relative, "/profile/episodes/check/{0}", episodeID);

            await _innerClient.SendAsync(uri, cancellationToken, _authorizator);
        }

        public async Task UncheckEpisodeAsync(int episodeID, CancellationToken cancellationToken = default(CancellationToken))
        {
            await _innerClient.SendAsync(UriBuilder.Build(UriKind.Relative,
                "/profile/episodes/uncheck/{0}", episodeID), cancellationToken, _authorizator);
        }

        public async Task SyncWatchedEpisodesAsync(int showID, IEnumerable<int> episodeIDs, CancellationToken cancellationToken = default(CancellationToken))
        {
            await _innerClient.SendAsync(UriBuilder.Build(UriKind.Relative,
                "/profile/shows/{0}/sync?episodes={1}", showID, string.Join(",", episodeIDs)),
                cancellationToken, _authorizator);
        }

        public async Task SyncEpisodesAsync(int showID,
            IEnumerable<int> checkEpisodeIDs, IEnumerable<int> uncheckEpisodeIDs,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            await _innerClient.SendAsync(UriBuilder.Build(UriKind.Relative, "/profile/shows/{0}/episodes?check={1}&uncheck={2}",
                showID, checkEpisodeIDs != null ? string.Join(",", checkEpisodeIDs) : string.Empty,
                uncheckEpisodeIDs != null ? string.Join(",", uncheckEpisodeIDs) : string.Empty),
                cancellationToken, _authorizator);
        }

        public async Task SetShowStatusAsync(int showID, SerialStatus status, CancellationToken cancellationToken = default(CancellationToken))
        {
            var strStatus = Enum.GetName(typeof(SerialStatus), status).ToLower();
            await _innerClient.SendAsync(UriBuilder.Build(UriKind.Relative,
                "/profile/shows/{0}/{1}", showID, strStatus),
                cancellationToken, _authorizator);
        }

        public async Task RateShowAsync(int showID, int rating, CancellationToken cancellationToken = default(CancellationToken))
        {
            await _innerClient.SendAsync(UriBuilder.Build(UriKind.Relative,
                "/profile/shows/{0}/rate/{1}", showID, rating),
                cancellationToken, _authorizator);
        }

        public async Task RateEpisodeAsync(int episodeID, int rating, CancellationToken cancellationToken = default(CancellationToken))
        {
            await _innerClient.SendAsync(UriBuilder.Build(UriKind.Relative,
                "/profile/episodes/rate/{0}/{1}", rating, episodeID), 
                cancellationToken, _authorizator);
        }

        public async Task<List<int>> GetFavoriteEpisodesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            return await _innerClient.GetObjectAsync<List<int>>(
                new Uri("/profile/episodes/favorites/list/", UriKind.Relative), 
                cancellationToken, _authorizator);
        }

        public async Task SetFavoriteEpisodeAsync(int episodeID, EpisodeAction action, CancellationToken cancellationToken = default(CancellationToken))
        {
            string strAction = Enum.GetName(typeof(EpisodeAction), action).ToLower();
            await _innerClient.SendAsync(UriBuilder.Build(UriKind.Relative,
                "/profile/episodes/favorites/{0}/{1}", strAction, episodeID), 
                cancellationToken, _authorizator);
        }

        public async Task<List<int>> GetIgnoredEpisodesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            return await _innerClient.GetObjectAsync<List<int>>(
                new Uri("/profile/episodes/ignored/list/", UriKind.Relative), 
                cancellationToken, _authorizator);
        }

        public async Task SetIgnoredEpisodeAsync(int episodeID, EpisodeAction action, CancellationToken cancellationToken = default(CancellationToken))
        {
            string strAction = Enum.GetName(typeof(EpisodeAction), action).ToLower();
            await _innerClient.SendAsync(UriBuilder.Build(UriKind.Relative,
                "/profile/episodes/ignored/{0}/{1}", strAction, episodeID), 
                cancellationToken, _authorizator);
        }

        public async Task<Dictionary<DateTime, List<NewsItem>>> GetNewsAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            var data = await _innerClient.GetObjectAsync<Dictionary<string, List<NewsItem>>>(
                new Uri("/profile/news/", UriKind.Relative),
                cancellationToken, _authorizator);

            var result = new Dictionary<DateTime, List<NewsItem>>();
            foreach (var item in data)
            {
                var date = DateTimeConverter.Parse(item.Key);
                result.Add(date, item.Value);
            }

            return result;
        }
        #endregion


        private static HttpClient _sharedClient;

        #region Unauthorized requests
        public static async Task<Dictionary<int, SearchShow>> SearchShowAsync(string query, CancellationToken cancellationToken = default(CancellationToken))
        {
            var result = await _sharedClient.GetObjectAsync<Dictionary<int, SearchShow>>(
                UriBuilder.Build(UriKind.Relative,
                "/shows/search/?q={0}", Uri.EscapeDataString(query)), cancellationToken);
            return result;
        }

        public static async Task<Show> GetShowAsync(int showID, CancellationToken cancellationToken = default(CancellationToken))
        {
            return await _sharedClient.GetObjectAsync<Show>(UriBuilder.Build(UriKind.Relative, 
                "/shows/{0}", showID), cancellationToken);
        }

        public static async Task<Dictionary<int, Genre>> GetGenresAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            var result = await _sharedClient.GetObjectAsync<Dictionary<int, Genre>>(
                new Uri("/genres/", UriKind.Relative), cancellationToken);
            return result;
        }

        public static async Task<List<TopShow>> GetTopShowsAsync(TopSerialGender gender, CancellationToken cancellationToken = default(CancellationToken))
        {
            string strGender = Enum.GetName(typeof(TopSerialGender), gender).ToLower();
            var result = await _sharedClient.GetObjectAsync<List<TopShow>>(UriBuilder.Build(UriKind.Relative,
                "/shows/top/{0}/", strGender), cancellationToken);
            return result;
        }

        public static async Task<Profile> GetProfileAsync(string login, CancellationToken cancellationToken = default(CancellationToken))
        {
            login.ThrowIfArgumentNullOrEmpry("login");

            var result = await _sharedClient.GetObjectAsync<Profile>(UriBuilder.Build(UriKind.Relative,
                "/profile/{0}", Uri.EscapeDataString(login)), cancellationToken);
            return result;
        }
        #endregion


       
    }
}
