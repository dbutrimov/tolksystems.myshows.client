﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TolkSystems.MyShows.Client
{
    public class MyShowsAuthorizator : IAuthorizator
    {
        private IAuthorization _authorization;
        private HttpClient _client;
        private HttpClientHandler _clientHandler;

        public MyShowsAuthorizator(IAuthorization authorization, HttpClient client, HttpClientHandler clientHandler)
        {
            authorization.ThrowIfArgumentNull("authorization");
            client.ThrowIfArgumentNull("client");
            clientHandler.ThrowIfArgumentNull("clientHandler");

            _authorization = authorization;
            _client = client;
            _clientHandler = clientHandler;
        }

        public Task AuthorizeAsync(CancellationToken cancellationToken)
        {
            return _authorization.AuthorizeAsync(_client, cancellationToken);
        }

        public async Task AuhtorizeIfNeedAsync(CancellationToken cancellationToken)
        {
            if (!_authorization.IsAuthorized(_client, _clientHandler))
                await _authorization.AuthorizeAsync(_client, cancellationToken);
        }

        public async Task<bool> AuthorizeIfUnauthorizedAsync(HttpResponseMessage response, CancellationToken cancellationToken)
        {
            if (!response.IsSuccessStatusCode)
            {
                if (response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    await AuthorizeAsync(cancellationToken);
                    return true;
                }
            }

            return false;
        }
    }
}
