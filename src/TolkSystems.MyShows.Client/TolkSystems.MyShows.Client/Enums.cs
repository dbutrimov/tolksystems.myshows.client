﻿using System;

namespace TolkSystems.MyShows.Client
{
    //public enum EpisodeType
    //{
    //    Unwatched,
    //    Next
    //}

    public enum SerialStatus
    {
        Watching,
        Later,
        Cancelled,
        Remove
    }

    public enum EpisodeAction
    {
        Add,
        Remove
    }

    public enum TopSerialGender
    {
        All,
        Male,
        Female
    }
}
