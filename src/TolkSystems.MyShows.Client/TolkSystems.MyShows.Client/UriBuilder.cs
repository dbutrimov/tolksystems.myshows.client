﻿using System;

namespace TolkSystems.MyShows.Client
{
    public static class UriBuilder
    {
        public static Uri Build(string format, params object[] args)
        {
            return new Uri(string.Format(format, args));
        }

        public static Uri Build(UriKind uriKind, string format, params object[] args)
        {
            return new Uri(string.Format(format, args), uriKind);
        }
    }
}
