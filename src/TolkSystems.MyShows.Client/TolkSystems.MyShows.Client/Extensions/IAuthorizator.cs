﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TolkSystems.MyShows.Client
{
    public interface IAuthorizator
    {
        Task AuthorizeAsync(CancellationToken cancellationToken);
        Task AuhtorizeIfNeedAsync(CancellationToken cancellationToken);
        Task<bool> AuthorizeIfUnauthorizedAsync(HttpResponseMessage response, CancellationToken cancellationToken);
    }
}
