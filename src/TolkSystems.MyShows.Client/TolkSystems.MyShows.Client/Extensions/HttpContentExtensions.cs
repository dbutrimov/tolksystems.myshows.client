﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace TolkSystems.MyShows.Client
{
    public static class HttpContentExtensions
    {
        public static async Task<T> ReadAsObjectAsync<T>(this HttpContent content)
            where T : class
        {
            content.ThrowIfArgumentNull("content");

            var json = await content.ReadAsStringAsync();

            json = json.EmptyDictionaryFix<T>();

            return await Task.Factory.StartNew<T>(() => JsonConvert.DeserializeObject<T>(json));

            //using (var stream = await content.ReadAsStreamAsync())
            //{
            //    using (var streamReader = new StreamReader(stream))
            //    {
            //        using (var reader = new JsonTextReader(streamReader))
            //        {
            //            var serializer = new JsonSerializer();
            //            return await Task.Factory.StartNew<T>(() => serializer.Deserialize<T>(reader));
            //        }
            //    }
            //}
        }

        private static string EmptyDictionaryFix<T>(this string json)
        {
            if (json == "[]")
            {
                var type = typeof(T);
                if (type.IsClass)
                    json = "{}";
            }

            return json;
        }
    }
}
