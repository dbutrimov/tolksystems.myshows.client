﻿using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace TolkSystems.MyShows.Client
{
    public static class HttpClientExtensions
    {
        //public static async Task<T> GetObjectAsync<T>(this HttpClient client, Uri requestUri)
        //    where T: class
        //{
        //    client.ThrowIfArgumentNull("client");
        //    requestUri.ThrowIfArgumentNull("requestUri");

        //    var response = await client.GetAsync(requestUri);
        //    response.ThrowIfNotSuccessStatusCode();

        //    return await response.Content.ReadAsObjectAsync<T>();
        //}

        


        public static Task<T> GetObjectAsync<T>(
            this HttpClient client, 
            Uri requestUri,
            CancellationToken cancellationToken = default(CancellationToken), 
            IAuthorizator authorizator = null)
            where T : class
        {
            return GetObjectAsync<T>(client, new HttpRequestMessage(HttpMethod.Get, requestUri), cancellationToken, authorizator);
        }

        public static async Task<T> GetObjectAsync<T>(
            this HttpClient client, 
            HttpRequestMessage request,
            CancellationToken cancellationToken = default(CancellationToken),
            IAuthorizator authorizator = null)
            where T : class
        {
            var response = await SendRequestAsync(client, request, cancellationToken, authorizator);
            return await response.Content.ReadAsObjectAsync<T>();
        }

        public static Task SendAsync(
            this HttpClient client,
            Uri requestUri,
            CancellationToken cancellationToken = default(CancellationToken),
            IAuthorizator authorizator = null)
        {
            return SendAsync(client, new HttpRequestMessage(HttpMethod.Get, requestUri), cancellationToken, authorizator);
        }

        public static Task SendAsync(
            this HttpClient client, 
            HttpRequestMessage request,
            CancellationToken cancellationToken = default(CancellationToken),
            IAuthorizator authorizator = null)
        {
            return SendRequestAsync(client, request, cancellationToken, authorizator);
        }


        public static async Task<HttpResponseMessage> SendRequestAsync(
           this HttpClient client,
           HttpRequestMessage request,
           CancellationToken cancellationToken = default(CancellationToken),
           IAuthorizator authorizator = null)
        {
            client.ThrowIfArgumentNull("client");
            request.ThrowIfArgumentNull("request");

            if (authorizator != null)
                await authorizator.AuhtorizeIfNeedAsync(cancellationToken);

            var response = await client.SendAsync(request, cancellationToken);
            if (authorizator != null)
            {
                if (await authorizator.AuthorizeIfUnauthorizedAsync(response, cancellationToken))
                    response = await client.SendAsync(request);
            }

            response.ThrowIfNotSuccessStatusCode();
            return response;
        }
    }
}
