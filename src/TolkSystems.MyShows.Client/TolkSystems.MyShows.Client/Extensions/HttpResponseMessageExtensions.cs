﻿using System;
using System.Net.Http;

namespace TolkSystems.MyShows.Client
{
    public static class HttpResponseMessageExtensions
    {
        public static void ThrowIfNotSuccessStatusCode(this HttpResponseMessage response)
        {
            if (!response.IsSuccessStatusCode)
                throw new HttpRequestException(response);
        }
    }
}
