﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace TolkSystems.MyShows.Client
{
	public class VkontakteAuthorization : CookieBasedAuthorization, IAuthorization
	{
        public string Token { get; private set; }
        public string UserId { get; private set; }

        public VkontakteAuthorization(string token, string userId)
        {
            token.ThrowIfArgumentNullOrEmpry("token");
            userId.ThrowIfArgumentNullOrEmpry("userId");

            Token = token;
            UserId = userId;
        }

        public override async Task AuthorizeAsync(HttpClient client, CancellationToken cancellationToken)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, new Uri("/profile/login/vk", UriKind.Relative));
            request.Content = new FormUrlEncodedContent(new Dictionary<string, string>() 
            {
                { "token", Token },
                { "userId", UserId }
            });

            var response = await client.SendAsync(request, cancellationToken);
            response.ThrowIfNotSuccessStatusCode();
        }
	}
}
