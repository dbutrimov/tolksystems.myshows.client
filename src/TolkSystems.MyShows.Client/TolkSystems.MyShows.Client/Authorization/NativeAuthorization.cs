﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace TolkSystems.MyShows.Client
{
	public class NativeAuthorization : CookieBasedAuthorization, IAuthorization
	{
		public string Login { get; private set; }
		public string Password { get; private set; }

        public NativeAuthorization(string login, string password)
        {
            login.ThrowIfArgumentNullOrEmpry("login");
            password.ThrowIfArgumentNullOrEmpry("password");

            Login = login;
            Password = password;
        }

        public override async Task AuthorizeAsync(HttpClient client, CancellationToken cancellationToken)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, new Uri("/profile/login", UriKind.Relative));
            request.Content = new FormUrlEncodedContent(new Dictionary<string, string>()
            {
                { "login", Login },
                { "password", Password }
            });

            var response = await client.SendAsync(request, cancellationToken);
            response.ThrowIfNotSuccessStatusCode();
        }
	}
}
