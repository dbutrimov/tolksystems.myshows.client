﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace TolkSystems.MyShows.Client
{
	public class TwitterAuthorization : CookieBasedAuthorization, IAuthorization
	{
        public string Token { get; private set; }
        public string UserId { get; private set; }
        public string Secret { get; set; }

        public TwitterAuthorization(string token, string userId, string secret)
        {
            token.ThrowIfArgumentNullOrEmpry("token");
            userId.ThrowIfArgumentNullOrEmpry("userId");
            secret.ThrowIfArgumentNullOrEmpry("secret");

            Token = token;
            UserId = userId;
            Secret = secret;
        }

        public override async Task AuthorizeAsync(HttpClient client, CancellationToken cancellationToken)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, new Uri("/profile/login/tw", UriKind.Relative));
            request.Content = new FormUrlEncodedContent(new Dictionary<string, string>()
            {
                { "token", Token },
                { "userId", UserId },
                { "secret", Secret }
            });

            var response = await client.SendAsync(request, cancellationToken);
            response.ThrowIfNotSuccessStatusCode();
        }
	}
}
