﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TolkSystems.Core;

namespace TolkSystems.MyShows.Client
{
    public abstract class CookieBasedAuthorization : IAuthorization
    {
        public abstract Task AuthorizeAsync(HttpClient client, CancellationToken cancellationToken);

        public bool IsAuthorized(HttpClient client, HttpClientHandler handler)
        {
            client.ThrowIfArgumentNull("client");
            handler.ThrowIfArgumentNull("handler");

            if (handler.CookieContainer.IsExpired(client.BaseAddress))
                return false;

            return true;
        }
    }
}
