﻿using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace TolkSystems.MyShows.Client
{
	public interface IAuthorization
	{
        Task AuthorizeAsync(HttpClient client, CancellationToken cancellationToken);
        bool IsAuthorized(HttpClient client, HttpClientHandler handler);
	}
}
