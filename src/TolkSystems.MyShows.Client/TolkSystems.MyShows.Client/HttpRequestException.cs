﻿using System;
using System.Net.Http;

namespace TolkSystems.MyShows.Client
{
    public class HttpRequestException : Exception
    {
        public HttpResponseMessage Response { get; private set; }

        public HttpRequestException(HttpResponseMessage response)
        {
            Response = response;
        }
    }
}
